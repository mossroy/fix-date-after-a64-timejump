# Workaround A64 timejump

The Allwinner A64 CPUs have a "timejump" hardware issue.

Symptom: the date suddenly changes to around 95 years later. Apart from potential side effects on the running software, it can break the network (the device is not reachable on the network any more, probably because of a failed DHCP renewal).

This is a known issue, see https://patchwork.kernel.org/project/linux-arm-kernel/patch/20180511022751.9096-2-samuel@sholland.org/ . There is already some code to workaround that in linux kernel: at least https://github.com/torvalds/linux/commit/c950ca8c35eeb32224a63adc47e12f9e226da241 and https://github.com/torvalds/linux/commit/8b33dfe0ba1c84c1aab2456590b38195837f1e6e. Olimex had also added some custom patches in their images, I don't know if it's still the case.

The kernel workarounds solve the issue 99% of the time. But it still happens from time to time on my Olinuxino A64 devices (maybe around once in a year).

This script is not a fix, and does not replace or improve the kernel workarounds. It's just a dirty workaround to avoid completely loosing the network if it happens (which is a big issue when the device is used as a headless server).

It stores the current date in a file, and (on next run) compares it with the (new) current date. If it is too far in the future (more than 90 years), it reverts the date to the one of the file.

It is strongly inspired from https://git.einval.com/cgi-bin/gitweb.cgi?p=fake-hwclock.git from Steve McIntyre

## Installation

The script has to be run by a user that has the rights to create the file (by default in /etc), and the rights to update the date/time with `date` command-line.

It should be run regularly (to minimize the risk to have a network failure), with crontab for example.

A quick-and-dirty way to do that is to put this script in `/etc/cron.hourly/` (it has to be set as executable)

## License

GPL v2
